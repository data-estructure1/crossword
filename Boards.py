#Board 1
animales = {
    "perro": "Animal doméstico comúnmente utilizado como mascota",
    "gato": "Animal doméstico que suele cazar ratones",
    "elefante": "Mamífero terrestre de gran tamaño con una trompa distintiva",
    "jirafa": "Animal de cuello largo y patas largas que se encuentra en África",
    "tigre": "Gran felino naranja y negro con rayas",
    "pingüino": "Ave marina no voladora que vive en la Antártida",
    "mono": "Primate de cuerpo pequeño y cola larga",
    "oso": "Mamífero carnívoro que vive en bosques y montañas",
    "león": "Gran felino amarillo con una melena distintiva",
    "canguro": "Marsupial saltador que se encuentra en Australia"
}

# Board 2
electrodomesticos = {
    "refrigerador": "Electrodoméstico utilizado para conservar alimentos y bebidas en frío",
    "lavadora": "Electrodoméstico que se utiliza para lavar la ropa",
    "televisor": "Dispositivo utilizado para recibir señales de televisión y mostrar imágenes y sonido",
    "licuadora": "Electrodoméstico utilizado para mezclar y triturar alimentos",
    "microondas": "Electrodoméstico que utiliza ondas electromagnéticas para calentar y cocinar alimentos",
    "aspiradora": "Dispositivo utilizado para limpiar superficies aspirando el polvo y la suciedad",
    "horno": "Electrodoméstico utilizado para cocinar y hornear alimentos",
    "cafetera": "Dispositivo utilizado para preparar café",
    "batidora": "Electrodoméstico utilizado para mezclar ingredientes y preparar masas",
    "plancha": "Electrodoméstico utilizado para alisar y eliminar arrugas de la ropa"
}
# Board 3
marcas = {
    "Apple": "Marca conocida por sus productos electrónicos como el iPhone, iPad y MacBook",
    "Samsung": "Marca surcoreana que produce una amplia gama de productos electrónicos y electrodomésticos",
    "Nike": "Marca reconocida mundialmente en la fabricación de calzado, ropa y accesorios deportivos",
    "Adidas": "Marca alemana especializada en la fabricación de calzado, ropa y accesorios deportivos",
    "Sony": "Marca japonesa que fabrica productos electrónicos como televisores, cámaras y consolas de videojuegos",
    "Microsoft": "Empresa de tecnología que desarrolla software y hardware, conocida por su sistema operativo Windows",
    "Coca-Cola": "Marca famosa por su bebida gaseosa de cola",
    "Toyota": "Marca japonesa de automóviles y vehículos comerciales",
    "Nike": "Marca reconocida mundialmente en la fabricación de calzado, ropa y accesorios deportivos",
    "Mercedes-Benz": "Marca alemana de automóviles de lujo y vehículos comerciales"
}

# Board 4

valores = {
    "amor": "Sentimiento de afecto profundo hacia otra persona",
    "honestidad": "Virtud de actuar con sinceridad y veracidad",
    "solidaridad": "Actitud de apoyo y ayuda hacia quienes lo necesitan",
    "respeto": "Consideración y reconocimiento hacia los demás",
    "tolerancia": "Capacidad de aceptar y respetar las diferencias de opinión o creencias",
    "justicia": "Principio de dar a cada uno lo que le corresponde",
    "humildad": "Virtud de reconocer las propias limitaciones y actuar con modestia",
    "libertad": "Estado de poder actuar y pensar sin restricciones",
    "paz": "Estado de tranquilidad y ausencia de conflictos",
    "responsabilidad": "Cumplimiento de los compromisos y obligaciones"
}

#  Board 4

prendas_vestir = {
    "camiseta": "Prenda de vestir de manga corta o larga, generalmente de algodón",
    "pantalón": "Prenda que cubre las piernas desde la cintura hasta los tobillos",
    "vestido": "Prenda de una o varias piezas que cubre el cuerpo y llega hasta cierta altura",
    "chaqueta": "Prenda de vestir que cubre el torso y generalmente tiene mangas",
    "falda": "Prenda que cubre la parte inferior del cuerpo, desde la cintura hasta los muslos o rodillas",
    "calcetines": "Prenda que se usa para cubrir y proteger los pies",
    "sombrero": "Accesorio que se lleva en la cabeza para protegerse del sol o como adorno",
    "zapatos": "Calzado que cubre el pie y puede tener diferentes estilos y diseños",
    "gafas": "Accesorio que se usa para proteger los ojos o como complemento estético",
    "bufanda": "Prenda larga y estrecha que se utiliza para abrigarse el cuello y el pecho"
}

# Board 5
deportes = {
    "fútbol": "Deporte en el que dos equipos compiten por marcar más goles en la portería contraria",
    "baloncesto": "Deporte en el que dos equipos intentan marcar puntos lanzando una pelota a través de un aro",
    "tenis": "Deporte en el que dos jugadores o parejas se enfrentan golpeando una pelota con raquetas",
    "natación": "Actividad deportiva que consiste en nadar en piscinas, ríos, mares u otros cuerpos de agua",
    "atletismo": "Conjunto de disciplinas deportivas que incluyen carreras, saltos y lanzamientos",
    "béisbol": "Deporte en el que dos equipos se enfrentan tratando de anotar carreras golpeando una pelota con un bate",
    "voleibol": "Deporte en el que dos equipos compiten golpeando una pelota por encima de una red",
    "gimnasia": "Actividad deportiva que involucra movimientos acrobáticos, fuerza y flexibilidad",
    "ciclismo": "Deporte en el que se compite o se realiza ejercicio en bicicleta",
    "boxeo": "Deporte de combate en el que dos oponentes se enfrentan utilizando los puños"
}





