import random
from prettytable import PrettyTable
animales = {
    "perro": "Animal doméstico comúnmente utilizado como mascota",
    "gato": "Animal doméstico que suele cazar ratones",
    "elefante": "Mamífero terrestre de gran tamaño con una trompa distintiva",
    "jirafa": "Animal de cuello largo y patas largas que se encuentra en África",
    "tigre": "Gran felino naranja y negro con rayas",
    "pingüino": "Ave marina no voladora que vive en la Antártida",
    "mono": "Primate de cuerpo pequeño y cola larga",
    "oso": "Mamífero carnívoro que vive en bosques y montañas",
    "león": "Gran felino amarillo con una melena distintiva",
    "canguro": "Marsupial saltador que se encuentra en Australia"
}


class Letter:
    def __init__(self, letter):
        self.letter = letter
        self.prev = None
        self.next = None


class Word:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def addWordLetterByLetter(self, word):
        for letter in word:
            self.addLetter(letter)

    def addLetter(self, value):
        newLetter = Letter(value)
        if self.head is None:
            self.head = newLetter
            self.tail = newLetter
        else:
            newLetter.prev = self.tail
            self.tail.next = newLetter
            self.tail = newLetter
        self.length += 1

    def printList(self):
        currentLetter = self.head
        print(" ")
        while currentLetter is not None:
            print(currentLetter.letter, end="-")
            currentLetter = currentLetter.next


class CrosswordTable:
    def __init__(self):
        self.table = PrettyTable()
        self.table.field_names = [" ", 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

        for i in range(1, 27):
            row = [str(i)] + [""] * 26
            self.table.add_row(row)

    def __str__(self):
        return str(self.table)

    def addLetter(self, row, column, letter):
        row_index = row - 1
        column_index = ord(column.upper()) - ord('A') + 1
        current_row = self.table._rows[row_index]
        current_row[column_index] = letter
        self.table._rows[row_index] = current_row

    def getValue(self, row, column):
        column_index = self.table.field_names.index(column)
        row_index = int(row) - 1
        return self.table.get_string(fields=[column_index])[row_index]

    def __str__(self):
        return str(self.table)


# Crear una lista de palabras en forma de listas doblemente enlazadas
words = []
for word, definition in animales.items():
    new_word = Word()
    new_word.addWordLetterByLetter(word)
    words.append((new_word, definition))

# Mezclar la lista de palabras en orden aleatorio
random.shuffle(words)

# Crear instancia de la tabla
crossword_table = CrosswordTable()

# Agregar las palabras en la tabla
row_index = 1
half_length = len(words) // 2  # Obtener la mitad de la longitud de las palabras
for i, (word, definition) in enumerate(words):
    currentLetter = word.head

    # Determinar la dirección: horizontal o vertical
    if i < half_length:
        # Palabra horizontal
        start_column = random.randint(1, 26 - word.length + 1)  # Generar columna inicial aleatoria
        column_index = start_column
        while currentLetter is not None:
            crossword_table.addLetter(row_index, crossword_table.table.field_names[column_index], currentLetter.letter)
            currentLetter = currentLetter.next
            column_index += 1
        row_index += 1

        # Reiniciar la columna al llegar a la última columna
        if column_index > 26:
            column_index = 1
    else:
        # Palabra vertical
        start_row = random.randint(1, 27 - word.length + 1)  # Generar fila inicial aleatoria
        row_index = start_row
        while currentLetter is not None:
            crossword_table.addLetter(row_index, crossword_table.table.field_names[column_index], currentLetter.letter)
            currentLetter = currentLetter.next
            row_index += 1
        column_index += 1

# Imprimir la tabla resultante
print(crossword_table)
