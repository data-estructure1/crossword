class Player:
    def __init__(self, username):
        self.name = username
        self.attempts = 3
        self.score = 0

    def displayPoints(self):
        print("\n\n" + self.name.upper() + "\nTu tienes" +
              str(self.attempts) + " Intentos restantes \nTu puntaje actual es: " + str(self.score) + ".\n")

    def addPoint(self):
        self.score += 1

    def subtractAttempts(self):
        self.attempts -= 1

    def displayWinner(self):
        print("\n\n" + self.name.upper() + "\nTu tienes  " +
              str(self.attempts) + " Intentos restantes \nTu puntaje final es " + str(self.score) + ".\n")

    def displayLoser(self):
        print("\n\n" + self.name.upper() +
              "\nTe quedaste sin intentos \nTu puntaje final es  " + str(self.score) + ".\n")

#We declare our class for queues
class Players:
    def __init__(self):
        self.usersPlayers = []
        
        
    #Verify list contents
    def is_empty(self):
        return len(self.usersPlayers) == 0
        
    def dequeue(self):
        if not self.is_empty():
            return self.usersPlayers.pop(0)
            
    def front(self):
        if not self.is_empty():
            return self.usersPlayers[0]
    
    def rear(self):
        if not self.is_empty():
            return self.usersPlayers[-1]
            
    def size(self):
        return len(self.usersPlayers)
            
    def print_items(self):
        print(self.usersPlayers)

    def getCurrentUser(self):
        if not self.is_empty():
            return self.usersPlayers[0]
        else:
            return None
            
    def getLastUser(self):
        if not self.is_empty():
            return self.usersPlayers[-1]
        else:
            return None

    def createUser(self):
        name = input("Name for user: ")
        naccount = input("Enter your account number: ")
        pin = input("Enter your PIN:")
        user = (name, naccount, pin)
        self.usersPlayers.append(user)
        
request = Players()    
request.createUser()
request.createUser()
request.createUser()
    
print("__________Usuarios en Cola__________")  

print("Number of users in queue: ", request.size())    
print("Current user in the cashier: ", request.getCurrentUser())

#Remove the current user in the cashier
request.dequeue()

# We print the number of users in the queue again.
print("Number of users in queue: ", request.size())
print("Last user in the queue: ", request.getLastUser())


    
                      
