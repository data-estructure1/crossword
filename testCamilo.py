from prettytable import PrettyTable

animales = {
    "perro": "Animal doméstico comúnmente utilizado como mascota",
    "gato": "Animal doméstico que suele cazar ratones",
    "elefante": "Mamífero terrestre de gran tamaño con una trompa distintiva",
    "jirafa": "Animal de cuello largo y patas largas que se encuentra en África",
    "tigre": "Gran felino naranja y negro con rayas",
    "pingüino": "Ave marina no voladora que vive en la Antártida",
    "mono": "Primate de cuerpo pequeño y cola larga",
    "oso": "Mamífero carnívoro que vive en bosques y montañas",
    "león": "Gran felino amarillo con una melena distintiva",
    "canguro": "Marsupial saltador que se encuentra en Australia"
}


class Letter:
    def __init__(self, letter):
        self.letter = letter
        self.prev = None
        self.next = None


class Word:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def addWordLetterByLetter(self, word):
        for letter in word:
            self.addLetter(letter)

    def addLetter(self, value):
        newLetter = Letter(value)
        if self.head is None:
            self.head = newLetter
            self.tail = newLetter
        else:
            newLetter.prev = self.tail
            self.tail.next = newLetter
            self.tail = newLetter
        self.length += 1

    def printList(self):
        currentLetter = self.head
        print(" ")
        while currentLetter is not None:
            print(currentLetter.letter, end="-")
            currentLetter = currentLetter.next





class CrosswordTable:
    def __init__(self):
        self.table = PrettyTable()
        self.table.field_names = [" ", 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

        for i in range(1, 27):
            row = ["" + str(i)]
            for j in range(1, 27):
                row.append("")
            self.table.add_row(row)

    def __str__(self):
        return str(self.table)


# Ejemplo de uso
crossword_table = CrosswordTable()
print(crossword_table)
