import random
from prettytable import PrettyTable
animales = {
    "perro": "Animal doméstico comúnmente utilizado como mascota",
    "gato": "Animal doméstico que suele cazar ratones",
    "elefante": "Mamífero terrestre de gran tamaño con una trompa distintiva",
    "jirafa": "Animal de cuello largo y patas largas que se encuentra en África",
    "tigre": "Gran felino naranja y negro con rayas",
    "pingüino": "Ave marina no voladora que vive en la Antártida",
    "mono": "Primate de cuerpo pequeño y cola larga",
    "oso": "Mamífero carnívoro que vive en bosques y montañas",
    "león": "Gran felino amarillo con una melena distintiva",
    "canguro": "Marsupial saltador que se encuentra en Australia"
}


class Letter:
    def __init__(self, letter):
        self.letter = letter
        self.prev = None
        self.next = None


class Word:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def addWordLetterByLetter(self, word):
        for letter in word:
            self.addLetter(letter)

    def addLetter(self, value):
        newLetter = Letter(value)
        if self.head is None:
            self.head = newLetter
            self.tail = newLetter
        else:
            newLetter.prev = self.tail
            self.tail.next = newLetter
            self.tail = newLetter
        self.length += 1

    def printList(self):
        currentLetter = self.head
        print(" ")
        while currentLetter is not None:
            print(currentLetter.letter, end="-")
            currentLetter = currentLetter.next


class CrosswordTable:
    def __init__(self):
        self.table = PrettyTable()
        self.table.field_names = [" ", 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

        for i in range(1, 27):
            row = [str(i)] + [""] * 26  # Inicialmente todas las celdas están vacías
            self.table.add_row(row)

    def __str__(self):
        return str(self.table)

    def addLetter(self, row, column, letter):
        row_index = row - 1
        column_index = ord(column.upper()) - ord('A') + 1
        current_row = self.table._rows[row_index]
        current_row[column_index] = letter
        self.table._rows[row_index] = current_row

    def getValue(self, row, column):
        column_index = self.table.field_names.index(column)
        row_index = int(row) - 1
        return self.table.get_string(fields=[column_index])[row_index]

    def __str__(self):
        return str(self.table)


# Crear una lista de palabras en forma de listas doblemente enlazadas
words = []
for word, definition in animales.items():
    new_word = Word()
    new_word.addWordLetterByLetter(word)
    words.append((new_word, definition))

# Mezclar la lista de palabras en orden aleatorio
random.shuffle(words)

# Crear instancia de la tabla
crossword_table = CrosswordTable()

# Obtener las palabras que se deben adivinar
words_to_guess = words

# Obtener las coordenadas de inicio para las palabras
start_coordinates = [(3, 2), (5, 4), (7, 6), (9, 8), (11, 10), (13, 12), (15, 14), (17, 16), (19, 18), (21, 20)]

# Agregar las palabras en la tabla
for i in range(len(words_to_guess)):
    word, _ = words_to_guess[i]
    start_row, start_column = start_coordinates[i]
    current_letter = word.head
    row_index = start_row
    column_index = start_column
    while current_letter is not None:
        crossword_table.addLetter(row_index, crossword_table.table.field_names[column_index], "X")
        current_letter = current_letter.next
        column_index += 1
    row_index += 1

# Imprimir la tabla inicial
print(crossword_table)

# Juego: adivinar las letras
total_letters = sum([word.length for word, _ in words_to_guess])
guessed_letters = 0
while guessed_letters < total_letters:
    coordinate = input("Ingresa las coordenadas (fila y columna) separadas por espacio: ")
    row, column = coordinate.split()
    row = int(row)
    column = column.upper()

    letter = input("Ingresa una letra: ").upper()
    found_letter = False

    for i in range(len(words_to_guess)):
        word, _ = words_to_guess[i]
        start_row, start_column = start_coordinates[i]
        current_letter = word.head
        row_index = start_row
        column_index = start_column
        while current_letter is not None:
            if row_index == row and crossword_table.table.field_names[column_index] == column:
                if current_letter.letter == letter:
                    crossword_table.addLetter(row_index, crossword_table.table.field_names[column_index], letter)
                    guessed_letters += 1
                    found_letter = True
                else:
                    print("Incorrecto. Inténtalo de nuevo.")
            current_letter = current_letter.next
            column_index += 1
        row_index += 1

    if not found_letter:
        print("Incorrecto. Inténtalo de nuevo.")

    print(crossword_table)

# Imprimir la tabla final con todas las letras reveladas
print("¡Resuelto!")
print("La tabla final es:")
print(crossword_table)
