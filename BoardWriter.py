from prettytable import PrettyTable

animales = {
    "perro": "Animal doméstico comúnmente utilizado como mascota",
    "gato": "Animal doméstico que suele cazar ratones",
    "elefante": "Mamífero terrestre de gran tamaño con una trompa distintiva",
    "jirafa": "Animal de cuello largo y patas largas que se encuentra en África",
    "tigre": "Gran felino naranja y negro con rayas",
    "pingüino": "Ave marina no voladora que vive en la Antártida",
    "mono": "Primate de cuerpo pequeño y cola larga",
    "oso": "Mamífero carnívoro que vive en bosques y montañas",
    "león": "Gran felino amarillo con una melena distintiva",
    "canguro": "Marsupial saltador que se encuentra en Australia"
}


class Letter:
    def __init__(self, letter):
        self.letter = letter
        self.prev = None
        self.next = None


class Word:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def addWordLetterByLetter(self, word):
        for letter in word:
            self.addLetter(letter)

    def addLetter(self, value):
        newLetter = Letter(value)
        if self.head is None:
            self.head = newLetter
            self.tail = newLetter
        else:
            newLetter.prev = self.tail
            self.tail.next = newLetter
            self.tail = newLetter
        self.length += 1

    def printList(self):
        currentLetter = self.head
        print(" ")
        while currentLetter is not None:
            print(currentLetter.letter, end="-")
            currentLetter = currentLetter.next





from prettytable import PrettyTable


class CrosswordTable:
    def __init__(self):
        self.table = PrettyTable()
        self.table.field_names = [" ", 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

        for i in range(1, 27):
            row = [str(i)] + [""] * 26
            self.table.add_row(row)

    def __str__(self):
        return str(self.table)

    def anadirValor(self, row, column, value):
        row_index = row -1
        column_index = ord(column.upper()) - ord('A') + 1
        current_row = self.table._rows[row_index]
        current_row[column_index] = value
        self.table._rows[row_index] = current_row

    def obtenerValor(self, row, column):
        column_index = self.table.field_names.index(column)
        row_index = int(row) - 1
        return self.table.get_string(fields=[column_index])[row_index]

    def __str__(self):
        return str(self.table)


# Ejemplo de uso
#crossword_table = CrosswordTable()
#crossword_table.anadirValor(3, 'B', 'X')
#print(crossword_table)









