from prettytable import PrettyTable
"""
class CrossWordBoard:
    def __init__(self):
        self.letters = ['  A', ' B', ' C', ' D', ' E', ' F', ' G', ' H', ' I', ' J', ' K', ' L', ' M', ' N', ' O', ' P', ' Q', ' R', ' S', ' T']
        self.numbers = [' 1', ' 2', ' 3', ' 4', ' 5', ' 6', ' 7', ' 8', ' 9', '10', '11', '12', '13', '14', '15', '16', '17', '18']
        self.board = [['X' for _ in range(len(self.letters))] for _ in range(len(self.numbers))]

    def print_board(self):
        max_len = max(len(str(num)) for num in self.numbers)  # Get the maximum length of the numbers

        print("  " + " ".join(self.letters))  # Print the letters aligned with the cells

        for number, row in zip(self.numbers, reversed(self.board)):
            row_str = [str(cell).rjust(max_len) if cell != "[X]" else "[X]" for cell in row]
            print(number + " " + " ".join(row_str))

    def set_value(self, position, value):
        letter, number = position[0], position[1:]
        index_letter = self.letters.index(letter.strip())
        index_number = self.numbers.index(number)
        self.board[index_number][index_letter] = value


crossword = CrossWordBoard()
crossword.print_board()

crossword.set_value(' C 4', 'F')

print("\nAfter setting value:")
crossword.print_board()
"""

class CrossWordBoard:
    def __init__(self):
        
        self.letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        self.numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18']
        self.board = [['[]' for _ in range(len(self.letters) )] for _ in range(len(self.numbers))]
    def print_board(self):

        my_table = PrettyTable()
        field_names = [];
        max_len = max(len(str(num)) for num in self.numbers)  # Get the maximum length of the numbers

        my_table.field_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']                                                          
        #print("  " + " ".join(self.letters))  # Print the letters aligned with the cells
            
        #number + " " + " ".join(row_str)

        for number, row in zip(self.numbers, reversed(self.board)):
            row_str = [str(cell).rjust(max_len) if cell != "X" else "X" for cell in row]
            my_table.add_row(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'])
            #print(number + " " + " ".join(row_str))
        print(my_table)
    def set_value(self, position, value):
        letter, number = position[0], position[1:]
        index_letter = self.letters.index(letter)
        index_number = self.numbers.index(number)
        self.board[index_number][index_letter] = value

crossword = CrossWordBoard()
crossword.print_board()

crossword.set_value('C4', '[F]')

print("\nAfter setting value:")
crossword.print_board()



